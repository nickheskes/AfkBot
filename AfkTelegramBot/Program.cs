﻿using System;
using AfkTelegramBot.Helpers;
using AfkTelegramBot.Services;

namespace AfkTelegramBot
{
    public class Program
    {
        private static bool _run = true;

        private static void Main(string[] args)
        {
            Console.WriteLine("Starting AFK chat bot");

            //Find API key
            var apiKey = Environment.GetEnvironmentVariable("TelegramApiKey");
            if (apiKey == null)
            {
                Console.Error.WriteLine("'TelegramApiKey' is not defined in environment. Exiting");
                Environment.Exit(1);
            }

            Console.WriteLine($"API key found ({apiKey.Substring(0, 10)}...)");
            TelegramService.Instance.Init(apiKey);

            //Test timezone crap
            Console.WriteLine($"Local time is: {TimeHelper.Now()}");
            Console.WriteLine($"First upcoming event wil occur at {TimedEventService.Instance.Next()}");

            while (_run)
            {
                var line = Console.ReadLine();
                if (line == "exit")
                    _run = false;
            }

            Console.WriteLine("Exiting");
            TimedEventService.Instance.Stop();
            TelegramService.Instance.Stop();
        }
    }
}