﻿using System;
using System.Runtime.InteropServices;

namespace AfkTelegramBot.Helpers
{
    public static class TimeHelper
    {
        private static TimeZoneInfo _nlTimeZone;

        public static DateTime Now()
        {
            if (_nlTimeZone == null)
                if (RuntimeInformation.IsOSPlatform(OSPlatform.Linux) || RuntimeInformation.IsOSPlatform(OSPlatform.Linux))
                    _nlTimeZone = TimeZoneInfo.FindSystemTimeZoneById("Europe/Amsterdam");
                else
                    _nlTimeZone = TimeZoneInfo.FindSystemTimeZoneById("W. Europe Standard Time");
            return TimeZoneInfo.ConvertTime(DateTime.UtcNow, _nlTimeZone);
            ;
        }
    }
}