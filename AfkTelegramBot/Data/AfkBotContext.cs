﻿using System;
using AfkTelegramBot.Model;
using Microsoft.EntityFrameworkCore;

namespace AfkTelegramBot.Data
{
    internal class AfkBotContext : DbContext
    {
        private static string _consStringCache = "Server=10.0.0.100;Database=AfkBot;User ID=AfkBot;Password=q[?d-AV[+s!4djB8;MultipleActiveResultSets=true;";
        public DbSet<TimedEvent> TimedEvents { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<ChatHistoryEntry> History { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            //Use env to keep shit out of git & push to docker

            if (_consStringCache == null)
            {
                var connStr = Environment.GetEnvironmentVariable("SqlServerConnStr");
                if (connStr == null)
                    throw new Exception("SQL Server connection string not found in env (key: SqlServerConnStr)");
                _consStringCache = connStr;
            }

            optionsBuilder.UseSqlServer(_consStringCache);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //No double entries
            modelBuilder.Entity<TimedEvent>().HasKey(c => new {c.Minute, c.Hour});
            modelBuilder.Entity<User>().HasKey(x => x.Id);
        }
    }
}