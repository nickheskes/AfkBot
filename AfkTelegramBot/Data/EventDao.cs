﻿using System.Linq;
using AfkTelegramBot.Model;

namespace AfkTelegramBot.Data
{
    public class EventDao
    {
        public TimedEvent[] TimedEvents;

        public EventDao()
        {
            SortAndCache();
        }

        public void AddOrEdit(TimedEvent timedEvent)
        {
            var db = new AfkBotContext();
            var existing = db.TimedEvents.FirstOrDefault(x => x.Hour == timedEvent.Hour && x.Minute == timedEvent.Minute);

            if (existing == null)
                db.TimedEvents.Add(timedEvent);
            else
                existing.Text = timedEvent.Text;

            db.SaveChanges();
            SortAndCache();
        }

        public bool Delete(int hour, int minute)
        {
            var db = new AfkBotContext();
            var foundAndRemoved = db.TimedEvents.Where(x => x.Hour == hour && x.Minute == minute);
            if (foundAndRemoved.Any())
            {
                foreach (var toREmove in foundAndRemoved)
                    db.History.RemoveRange(db.History.Where(x => x.TimedEvent == toREmove));
                db.SaveChanges();
                db.TimedEvents.RemoveRange(foundAndRemoved);
                db.SaveChanges();
                SortAndCache();
                return true;
            }

            return false;
        }

        private void SortAndCache()
        {
            var db = new AfkBotContext();
            TimedEvents = db.TimedEvents.OrderBy(x => x.Hour).ThenBy(x => x.Minute).ToArray();
        }
    }
}