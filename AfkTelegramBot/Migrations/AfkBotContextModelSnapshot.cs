﻿using System;
using AfkTelegramBot.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;

namespace AfkTelegramBot.Migrations
{
    [DbContext(typeof(AfkBotContext))]
    internal class AfkBotContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.1.2")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("AfkTelegramBot.Model.ChatHistoryEntry", b =>
            {
                b.Property<int>("Id")
                    .ValueGeneratedOnAdd();

                b.Property<DateTime>("DateTime");

                b.Property<string>("Message");

                b.Property<bool>("Score");

                b.Property<int?>("TimedEventHour");

                b.Property<int?>("TimedEventMinute");

                b.Property<int?>("UserId");

                b.HasKey("Id");

                b.HasIndex("UserId");

                b.HasIndex("TimedEventMinute", "TimedEventHour");

                b.ToTable("History");
            });

            modelBuilder.Entity("AfkTelegramBot.Model.TimedEvent", b =>
            {
                b.Property<int>("Minute");

                b.Property<int>("Hour");

                b.Property<string>("Text");

                b.HasKey("Minute", "Hour");

                b.ToTable("TimedEvents");
            });

            modelBuilder.Entity("AfkTelegramBot.Model.User", b =>
            {
                b.Property<int>("Id")
                    .ValueGeneratedOnAdd();

                b.Property<string>("Name");

                b.Property<int>("Score");

                b.Property<long>("TelegramId");

                b.HasKey("Id");

                b.ToTable("Users");
            });

            modelBuilder.Entity("AfkTelegramBot.Model.ChatHistoryEntry", b =>
            {
                b.HasOne("AfkTelegramBot.Model.User", "User")
                    .WithMany()
                    .HasForeignKey("UserId");

                b.HasOne("AfkTelegramBot.Model.TimedEvent", "TimedEvent")
                    .WithMany()
                    .HasForeignKey("TimedEventMinute", "TimedEventHour");
            });
        }
    }
}