﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace AfkTelegramBot.Migrations
{
    public partial class AutoMigrationsPls : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                "TimedEvents",
                table => new
                {
                    Minute = table.Column<int>(nullable: false),
                    Hour = table.Column<int>(nullable: false),
                    Text = table.Column<string>(nullable: true)
                },
                constraints: table => { table.PrimaryKey("PK_TimedEvents", x => new {x.Minute, x.Hour}); });

            migrationBuilder.CreateTable(
                "Users",
                table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true),
                    Score = table.Column<int>(nullable: false),
                    TelegramId = table.Column<long>(nullable: false)
                },
                constraints: table => { table.PrimaryKey("PK_Users", x => x.Id); });

            migrationBuilder.CreateTable(
                "History",
                table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    DateTime = table.Column<DateTime>(nullable: false),
                    Message = table.Column<string>(nullable: true),
                    Score = table.Column<bool>(nullable: false),
                    TimedEventHour = table.Column<int>(nullable: true),
                    TimedEventMinute = table.Column<int>(nullable: true),
                    UserId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_History", x => x.Id);
                    table.ForeignKey(
                        "FK_History_Users_UserId",
                        x => x.UserId,
                        "Users",
                        "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        "FK_History_TimedEvents_TimedEventMinute_TimedEventHour",
                        x => new {x.TimedEventMinute, x.TimedEventHour},
                        "TimedEvents",
                        new[] {"Minute", "Hour"},
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                "IX_History_UserId",
                "History",
                "UserId");

            migrationBuilder.CreateIndex(
                "IX_History_TimedEventMinute_TimedEventHour",
                "History",
                new[] {"TimedEventMinute", "TimedEventHour"});
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                "History");

            migrationBuilder.DropTable(
                "Users");

            migrationBuilder.DropTable(
                "TimedEvents");
        }
    }
}