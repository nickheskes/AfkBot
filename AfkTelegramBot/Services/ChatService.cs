﻿using System;
using System.Collections.Generic;
using System.Linq;
using AfkTelegramBot.Helpers;
using Telegram.Bot.Types;

namespace AfkTelegramBot.Services
{
    internal class ChatService
    {
        private static readonly HashSet<long> _subscribedChats = new HashSet<long>();
        private static ChatService _instance;

        protected ChatService()
        {
            Console.WriteLine("Starting chatservice");
        }

        public static ChatService Instance => _instance ?? (_instance = new ChatService());
        public long[] SubscribedChats => _subscribedChats.ToArray();

        public string Command(string cmd, Message msg)
        {
            if (cmd == "start")
            {
                if (SubscribedChats.Contains(msg.Chat.Id))
                    return $"Hi chat {msg.Chat.Id}, you are already subscribed";
                Console.WriteLine(msg.Chat.Id);
                _subscribedChats.Add(msg.Chat.Id);
                return $"Hi chat {msg.Chat.Id}, you will now receive spam";
            }
            return "Command not found";
        }
    }
}