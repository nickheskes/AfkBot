﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using AfkTelegramBot.Data;
using AfkTelegramBot.Helpers;
using Telegram.Bot;
using Telegram.Bot.Args;
using Telegram.Bot.Types;

namespace AfkTelegramBot.Services
{
    internal class TelegramService
    {
        private static TelegramBotClient _bot;
        private static TelegramService _instance;
        private static readonly List<int> Admins = new List<int> {64116902};
        private readonly Dictionary<int, List<DateTime>> _antiSpamProtection = new Dictionary<int, List<DateTime>>();
        private HashSet<int> _spamSet = new HashSet<int>(); 

        protected TelegramService()
        {
            Console.WriteLine("Starting Telegram Service");
        }

        public static TelegramService Instance => _instance ?? (_instance = new TelegramService());

        private void _bot_OnMessage(object sender, MessageEventArgs msMessageEventArgs)
        {
            var msg = msMessageEventArgs.Message;
            var isReply = true;
            string response = null;
            var cmd = msg.Text;

      
            if (cmd == null)
                return;


            Console.WriteLine("Message received " + cmd);
            if (cmd.StartsWith("/"))
            {
                ProcessCommand(cmd.Substring(1), msg);
            }
            else
                ProcessMessage(cmd, msg);
        }

        private void ProcessCommand(string cmd, Message msg)
        {
            if (_spamSet.Contains(msg.From.Id))
            {
                if (_antiSpamProtection[msg.From.Id].Count(x => x > DateTime.UtcNow.AddHours(-1)) == 0)
                {
                    _spamSet.Remove(msg.From.Id);
                    _antiSpamProtection[msg.From.Id].Clear();
                }
                else
                {
                    return;
                }
            }
            string response;
            var isAdmin = Admins.Contains(msg.From.Id);
            if (!isAdmin)
            {
                if (_antiSpamProtection.ContainsKey(msg.From.Id))
                {
                    if (_antiSpamProtection[msg.From.Id].Count(x => x > DateTime.UtcNow.AddHours(-1)) >= 3)
                    {
                        _bot.SendTextMessageAsync(msg.Chat.Id, $"You have hit your spam-limit!", disableNotification: true, replyToMessageId: msg.MessageId);
                        _spamSet.Add(msg.From.Id);
                        return;
                    }
                    _antiSpamProtection[msg.From.Id].Add(DateTime.UtcNow);
                }
                else
                {
                    _antiSpamProtection.Add(msg.From.Id, new List<DateTime>() { DateTime.UtcNow});
                }
            }

            if (cmd.StartsWith("leaderboard"))
            {
                var sb = new StringBuilder();
                var db = new AfkBotContext();

                sb.AppendLine($"Leaderboard");
                foreach (var user in db.Users)
                    sb.AppendLine($"{user.Name}: {user.Score} points");
                response = sb.ToString();
            }
            else if (cmd.StartsWith("servertime"))
            {
                response = TimeHelper.Now().ToString();
            }
            else if (cmd.StartsWith("help"))
            {
                response = "/event [list/next/add/remove] \n /leaderboard \n /servertime \n /chat start";
            }
            else if (cmd.StartsWith("chat"))
            {
                response = ChatService.Instance.Command(cmd.Substring(5), msg);
            }
            else if (cmd.StartsWith("event"))
            {
                response = TimedEventService.Instance.Command(cmd.Substring(6), msg, isAdmin);
            }
            else
            {
                response = "Command not found";
            }
            _bot.SendTextMessageAsync(msg.Chat.Id, response, disableNotification: true, replyToMessageId: msg.MessageId);
        }

        private void ProcessMessage(string cmd, Message msg)
        {
            if (TimedEventService.Instance.CheckMsgIsEvent(cmd, msg))
                return;
        }

        public void SendTextMessage(long chatId, string message)
        {
            if (_bot == null)
                throw new Exception("Service not initialized");

            _bot.SendTextMessageAsync(chatId, message, disableNotification: true);
        }

        public void ReplyTextMessage(long chatId, string message, int replyToMsgId)
        {
            if (_bot == null)
                throw new Exception("Service not initialized");

            _bot.SendTextMessageAsync(chatId, message, disableNotification: true, replyToMessageId: replyToMsgId);
        }

        public void Init(string apiKey)
        {
            _bot = new TelegramBotClient(apiKey);
            _bot.OnMessage += _bot_OnMessage;
            _bot.StartReceiving();
            Console.WriteLine($"Telegram service started, receiving {_bot.IsReceiving}");
        }

        public void Stop()
        {
            _bot.StopReceiving();
            _instance = null;
        }
    }
}