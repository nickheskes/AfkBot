﻿namespace AfkTelegramBot.Model
{
    public class TimedEvent
    {
        public TimedEvent(int hour, int minute, string text)
        {
            Hour = hour;
            Minute = minute;
            Text = text;
        }

        public TimedEvent()
        {
        }

        public int Minute { get; set; }
        public int Hour { get; set; }
        public string Text { get; set; }

        public override string ToString()
        {
            return $"{Hour:D2}:{Minute:D2} - {Text}";
        }
    }
}