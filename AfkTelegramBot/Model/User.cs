﻿using System.ComponentModel.DataAnnotations;

namespace AfkTelegramBot.Model
{
    public class User
    {
        [Key]
        public int Id { get; set; } //#IdentityInsert :'(
        public long TelegramId { get; set; }
        public string Name { get; set; }
        public int Score { get; set; }
    }
}