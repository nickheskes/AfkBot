﻿using System;

namespace AfkTelegramBot.Model
{
    public class ChatHistoryEntry
    {
        public int Id { get; set; }
        public User User { get; set; }
        public TimedEvent TimedEvent { get; set; }
        public string Message { get; set; }
        public DateTime DateTime { get; set; }
        public bool Score { get; set; }
    }
}