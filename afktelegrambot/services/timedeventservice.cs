﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using AfkTelegramBot.Data;
using AfkTelegramBot.Helpers;
using AfkTelegramBot.Model;
using Telegram.Bot.Types;
using User = AfkTelegramBot.Model.User;

namespace AfkTelegramBot.Services
{
    public class TimedEventService
    {
        private static TimedEventService _instance;
        private readonly EventDao _dao = new EventDao();
        private readonly Thread _timer;
        private readonly Dictionary<long, bool> ScoreChanges = new Dictionary<long, bool>();
        private DateTime _eventEnd;
        private Dictionary<string, TimedEvent> _eventTextCache;

        private bool _ongoingEvent;
        private bool _eventCalledTime;
        private bool _run = true;
        private int _timedEventIndex;

        protected TimedEventService()
        {
            FindFirstUpcoming();
            _timer = new Thread(CheckTimeThread) { IsBackground = true };
            _timer.Start();
        }

        public static TimedEventService Instance => _instance ?? (_instance = new TimedEventService());

        public string Command(string cmd, Message msg, bool isAdmin)
        {
            if (cmd.StartsWith("add"))
            {
                if (!isAdmin)
                {
                    return "You are not allowed to add events";
                }
                var match = Regex.Match(cmd, "add (?<hour>\\d{2}):(?<minute>\\d{2}) (?<text>.*)");

                if (!match.Success)
                    return "Invalid arguments. Syntax: add hh:mm text";

                var hour = Convert.ToInt32(match.Groups["hour"].Value);
                var minute = Convert.ToInt32(match.Groups["minute"].Value);
                var text = match.Groups["text"].Value;
                var timedEvent = new TimedEvent(hour, minute, text);
                _dao.AddOrEdit(timedEvent);
                FindFirstUpcoming();
                return $"Scheduled {timedEvent}";
            }

            if (cmd.StartsWith("list"))
            {
                var sb = new StringBuilder();
                foreach (var daoTimedEvent in _dao.TimedEvents)
                    sb.AppendLine(daoTimedEvent.ToString());
                return sb.ToString();
            }

            if (cmd.StartsWith("next"))
            {
                var next = Next();
                if (next == null)
                    return "No upcoming event";
                return Next().ToString();
            }

            if (cmd.StartsWith("leaderboard"))
            {
                var sb = new StringBuilder();
                var db = new AfkBotContext();
                sb.AppendLine($"Leaderboard{Environment.NewLine}-----------");
                foreach (var user in db.Users.OrderByDescending(x => x.Score))
                    sb.AppendLine($"{user.Name}: {user.Score}");
                return sb.ToString();
            }

            if (cmd.StartsWith("remove"))
            {
                if (!isAdmin)
                {
                    return "You are not allowed to remove events";
                }
                var match = Regex.Match(cmd, "remove (?<hour>\\d{2}):(?<minute>\\d{2})");

                if (!match.Success)
                    return "Invalid arguments. Syntax: remove hh:mm text";

                var hour = Convert.ToInt32(match.Groups["hour"].Value);
                var minute = Convert.ToInt32(match.Groups["minute"].Value);
                if (!match.Success)
                    return "Invalid arguments. Syntax: remove hh:mm";

                if (_dao.Delete(hour, minute))
                {
                    FindFirstUpcoming();
                    return $"Event at {hour}:{minute} - Removed";
                }
                return $"Event at {hour}:{minute} - Not found";
            } 

            return "Usage: /event [list/next/add/remove]";
        }

        public void AddOrEdit(TimedEvent timedEvent)
        {
            _dao.AddOrEdit(timedEvent);
            FindFirstUpcoming();
        }

        private void CheckTimeThread()
        {
            while (_run)
            {
                var now = TimeHelper.Now();
                if (!_ongoingEvent && _dao.TimedEvents.Any() &&
                    _dao.TimedEvents[_timedEventIndex].Hour == now.Hour &&
                    _dao.TimedEvents[_timedEventIndex].Minute == now.Minute)
                {
                    ScoreChanges.Clear();
                    _ongoingEvent = true;
                    _eventCalledTime = false;
                    _eventEnd = now.AddMinutes(1);
                    Console.WriteLine(_dao.TimedEvents[_timedEventIndex].ToString());


                } else if (!_eventCalledTime && now.Second > 15)
                {
                    foreach (var chatId in ChatService.Instance.SubscribedChats)
                        TelegramService.Instance.SendTextMessage(chatId, _dao.TimedEvents[_timedEventIndex].Text);
                    _eventCalledTime = true;
                }
                else if (_ongoingEvent && now > _eventEnd)
                {
                    _ongoingEvent = false;
                    var sb = new StringBuilder();
                    var db = new AfkBotContext();
                    sb.AppendLine($"Leaderboard changes for {_dao.TimedEvents[_timedEventIndex].Hour:D2}:{_dao.TimedEvents[_timedEventIndex].Minute:D2}{Environment.NewLine}-----------");
                    foreach (var user in db.Users.Where(x => ScoreChanges.Keys.ToList().Contains(x.Id)))
                    {
                        var x = ScoreChanges[user.Id] ? "+" : "-";
                        sb.AppendLine($"{user.Name}: {x}1 point. New score: {user.Score}");
                    }

                    if (_timedEventIndex + 1 == _dao.TimedEvents.Length)
                        _timedEventIndex = 0;
                    else
                        _timedEventIndex++;
                    foreach (var chatId in ChatService.Instance.SubscribedChats)
                        TelegramService.Instance.SendTextMessage(chatId, sb.ToString());
                }

                Thread.Sleep(5000);
            }
        }

        private void FindFirstUpcoming()
        {
            TimedEvent firstUpcoming = null;
            _timedEventIndex = 0;
            var diffUpcoming = 0;
            var now = TimeHelper.Now();

            var remainingToday = _dao.TimedEvents.Where(x =>
            x.Hour > now.Hour ||
            (x.Hour == now.Hour && x.Minute > now.Minute)).ToArray();
            if (remainingToday.Any())
            {
                foreach (var timedEvent in remainingToday)
                {
                    var diff = Math.Abs(Convert.ToInt32((now - now.Date.AddHours(timedEvent.Hour).AddMinutes(timedEvent.Minute)).TotalSeconds));
                    if (diffUpcoming == 0 || diff < diffUpcoming)
                    {
                        diffUpcoming = diff;
                        firstUpcoming = timedEvent;
                    }
                }
            }
            else if (_dao.TimedEvents.Length > 0)
            {
                firstUpcoming = _dao.TimedEvents[0];
            }
            else
            {
                //No events, not selecting anything
                return;
            }

            while (_dao.TimedEvents[_timedEventIndex] != firstUpcoming)
                _timedEventIndex++;
        }

        public void Stop()
        {
            _run = false;
        }

        public TimedEvent Next()
        {
            if (_dao.TimedEvents.Length == 0)
                return null;
            return _dao.TimedEvents[_timedEventIndex];
        }

        public bool CheckMsgIsEvent(string cmd, Message msg)
        {
            if (_dao.TimedEvents.Length == 0)
                return false;

            if (cmd.StartsWith("/"))
                cmd = cmd.Substring(1);

            var score = false;
            var now = TimeHelper.Now();
            var clientTime = msg.Date;

            int minute = now.Minute;
            if (clientTime.Minute != now.Minute)
                minute = clientTime.Minute;
            TimedEvent timedEvent = null;

            if (clientTime.Minute != now.Minute)
            {
                Console.WriteLine($"Time mismatch for {msg.Chat.Username}: {now - clientTime}");
            }

            //Try to find event corresponding to message content
            if (_dao.TimedEvents[_timedEventIndex].Text == cmd)
            {
                timedEvent = _dao.TimedEvents[_timedEventIndex];
            }
            else
            {
                timedEvent = _dao.TimedEvents.FirstOrDefault(x => x.Text == cmd);
            }


            // Message reference does not contain event
            if (timedEvent == null)
                return false;

            
            if (timedEvent == _dao.TimedEvents[_timedEventIndex] && timedEvent.Hour == now.Hour && timedEvent.Minute == minute)
                score = true; // Message contains event and event is upcoming event

            var db = new AfkBotContext();
            var user = db.Users.FirstOrDefault(x => x.TelegramId == msg.From.Id);
            if (user == null)
            {
                user = db.Users.Add(new User
                {
                    TelegramId = msg.From.Id,
                    Name = msg.From.Username,
                    Score = 0
                }).Entity;
                db.SaveChanges();
            }

            if (ScoreChanges.ContainsKey(user.Id))
            {
                if (!score)
                    TelegramService.Instance.ReplyTextMessage(msg.Chat.Id, $"It is still not {timedEvent.Hour:D2}:{timedEvent.Minute:D2} yet... You can keep your stupid points", msg.MessageId);
                return true;
            }

            user.Score = score ? user.Score + 1 : user.Score - 1;
            if (!score)
            {
                TelegramService.Instance.ReplyTextMessage(msg.Chat.Id, $"It is not {timedEvent.Hour:D2}:{timedEvent.Minute:D2} yet. You have lost 1 point (new score: {user.Score}) and you will not get any points the next round o/", msg.MessageId);
                if (!ScoreChanges.ContainsKey(user.Id))
                    ScoreChanges.Add(user.Id, false);
            }
            else
            {
                ScoreChanges.Add(user.Id, true);
            }

            //if (!score && now.Hour > timedEvent.Hour)
            //    now = now.AddDays(1); // No\t allowed to retry next attempt. Eg 420 call at 23:45. not allowd to call 420 next day

            db.History.Add(new ChatHistoryEntry
            {
                User = user,
                TimedEvent = db.TimedEvents.SingleOrDefault(x => x.Hour == timedEvent.Hour && x.Minute == timedEvent.Minute),
                Message = cmd,
                DateTime = now,
                Score = score
            });

            try
            {
                db.SaveChanges();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                //throw;
            }

            return true;
        }
    }
}